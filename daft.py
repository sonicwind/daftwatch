import pandas as pd
from daftlistings import (Daft, Location, MapVisualization, PropertyType,SearchType)

# Daft Options
daft = Daft()
daft.set_location([Location.GALWAY, Location.CLARE])
daft.set_search_type(SearchType.RESIDENTIAL_SALE)
daft.set_property_type(PropertyType.HOUSE)
daft.set_min_price(240000)
daft.set_max_price(375000)

# Search
listings = daft.search()

# Cache the listings in the local file
with open("result.txt", "w") as fp:
    fp.writelines("%s\n" % listing.as_dict_for_mapping() for listing in listings)

# Read from the cache
with open("result.txt") as fp:
  lines = fp.readlines()

properties = []
for line in lines:
  properties.append(eval(line))

# Pandas DataFrame
df = pd.DataFrame(properties)

# Setup Map
map = MapVisualization(df)
map.add_markers()
map.add_colorbar()
map.save("houses.html")
